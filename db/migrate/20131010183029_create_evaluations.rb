class CreateEvaluations < ActiveRecord::Migration
  def change
    create_table :evaluations do |t|
      t.integer :account_id
      t.integer :category_id
      t.integer :score

      t.timestamps
    end
  end
end
