class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.integer :user_id
      t.integer :assignee_id
      t.string :name
      t.string :status
      t.date :due_date

      t.timestamps
    end
  end
end
