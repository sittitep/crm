class AddEvaluationIdToFieldDetail < ActiveRecord::Migration
  def change
    add_column :field_details, :evaluation_id, :integer
  end
end
