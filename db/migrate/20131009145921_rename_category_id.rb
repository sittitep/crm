class RenameCategoryId < ActiveRecord::Migration
  def change
    rename_column :accounts, :category_id, :account_category_id
  end
end
