class CreateFieldDetails < ActiveRecord::Migration
  def change
    create_table :field_details do |t|
      t.integer :field_id
      t.integer :account_id
      t.string :value

      t.timestamps
    end
  end
end
