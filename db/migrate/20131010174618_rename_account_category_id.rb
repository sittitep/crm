class RenameAccountCategoryId < ActiveRecord::Migration
  def change
    rename_column :accounts, :account_category_id, :category_id
  end
end
