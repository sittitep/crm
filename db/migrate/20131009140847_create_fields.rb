class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|
      t.string :name
      t.integer :field_group_id

      t.timestamps
    end
  end
end
