class AddClassToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :category_class, :string
  end
end
