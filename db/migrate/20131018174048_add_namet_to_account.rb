class AddNametToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :name, :string
    add_column :evaluations, :name, :string
  end
end
