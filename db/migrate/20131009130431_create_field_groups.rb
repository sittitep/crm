class CreateFieldGroups < ActiveRecord::Migration
  def change
    create_table :field_groups do |t|
      t.string :name
      t.integer :category_id

      t.timestamps
    end
  end
end
