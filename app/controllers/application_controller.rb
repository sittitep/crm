class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :authenticate_user!
  before_filter :set_active_sidebar
  
  def set_active_sidebar
    @sidebar_active = controller_name
    if controller_name == "evaluations"
      @sidebar_active = "accounts"
      @menu_active = "category"
    elsif controller_name == "categories"
      @sidebar_active = "settings"
      @menu_active = "category"
    end
  end

  def add_user_id(entry)
    entry.user_id = current_user.id
  end
  
end
