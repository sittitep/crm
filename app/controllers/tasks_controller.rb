class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]
  after_action :send_mail, only: [:create, :update]

  def send_mail
    if @task.status == "Pending"
      TaskMailer.new_task(@task.assignee,@task).deliver
    elsif @task.status == "Completed"
      TaskMailer.completed_task(@task.user,@task).deliver
    end
  end

  # GET /tasks
  # GET /tasks.json
  def index
    @pending = Task.includes(:user).where(assignee_id: current_user.id, status: "Pending")
    @pending = {
      name: "pending",
      overdue: @pending.where("due_date < ?", Date.today),
      today: @pending.where("due_date = ?", Date.today),
      incoming: @pending.where("due_date > ?", Date.today),
      count: @pending.count,
    }
    @assigned = Task.includes(:user).where(user_id: current_user.id, status: "Pending")
    @assigned = {
      name: "assigned",
      overdue: @assigned.where("due_date < ?", Date.today),
      today: @assigned.where("due_date = ?", Date.today),
      incoming: @assigned.where("due_date > ?", Date.today),
      count: @assigned.count,
    }
    @completed = Task.where(status: "Completed")
    @completed = {
      name: "completed",
      my_works: @completed.where(assignee_id: current_user.id),
      friends_works: @completed.where("user_id = ? and assignee_id != ?", current_user.id, current_user.id)
    }
    @task = Task.new()
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)
    add_user_id(@task)
    @task.status = Setting.task_status.first
    
    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_path, notice: 'Task was successfully created.' }
        format.json { render action: 'show', status: :created, location: @task }
      else
        format.html { render action: 'new' }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to tasks_path, notice: 'Task was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    TaskMailer.canceled_task(@task.assignee,@task).deliver
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:user_id, :assignee_id, :name, :status, :due_date)
    end
end
