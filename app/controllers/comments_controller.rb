class CommentsController < ApplicationController
  def create
    @comment = Comment.new(comment_params)

    if @comment.save
      redirect_to "/#{@comment.commentable_type.downcase}s/#{@comment.commentable_id}"
    else

    end
  end
  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:user_id, :commentable_id, :commentable_type, :comment, :title)
      # params.permit!
    end
end
