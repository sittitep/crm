class AccountsController < ApplicationController

  before_action :set_account, only: [:show, :edit, :update, :destroy]
  before_action :new_account, only: [:index, :new, :create]
  
  
  # GET /accounts
  # GET /accounts.json
  def import
    Account.import(params[:file])
    redirect_to root_url, notice: "Products imported."
  end

  def index
    if params[:task] == "search"
      if params[:name].present?
        accounts = Account.where("name like ? and category_id = ?", "%#{params[:name]}%", params[:category_id].to_i)
      else
        accounts = Account.where(category_id: params[:category_id])
      end
      @accounts = accounts.map{ |account| {id: account.id, name: account.name, username: account.user.username, created_at: account.created_at.to_date.to_s(:long)}}
    else
      @accounts = Hash.new
      Category.where(category_class: "Account").each do |category|
        @accounts.merge!(category.name.to_sym => category.accounts.last(20)) 
      end
    end
    
    respond_to do |format|
      format.html
      format.js { render :json => @accounts }
      format.csv { send_data Account.to_csv(@accounts[params[:category].to_sym]) }
    end
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end

  # GET /accounts/new
  def new
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)
    add_user_id(@account)
    respond_to do |format|
      if @account.save
        field_groups = @account.category.field_groups
        field_groups.each do |fg|
          fg.fields.each do |f|
            field_detail = @account.field_details.create(field_id: f.id)
          end
        end

        format.html { redirect_to edit_account_path(@account, alert_type: 'alert alert-success'), notice: 'Account was successfully created.' }
        format.json { render action: 'show', status: :created, location: @account }
      else
        format.html { render action: 'new' }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to @account, notice: 'Account was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    def new_account
      @account = Account.new
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:file, :name, :category_id, field_details_attributes: [:id, :value])
    end


end
