class FieldGroupsController < ApplicationController
  before_action :set_field_group, only: [:show, :edit, :update, :destroy]

  # GET /field_groups
  # GET /field_groups.json
  def index
    @field_groups = FieldGroup.all
  end

  # GET /field_groups/1
  # GET /field_groups/1.json
  def show
  end

  # GET /field_groups/new
  def new
    @field_group = FieldGroup.new
  end

  # GET /field_groups/1/edit
  def edit
  end

  # POST /field_groups
  # POST /field_groups.json
  def create
    @field_group = FieldGroup.new(field_group_params)

    respond_to do |format|
      if @field_group.save
        format.html { redirect_to category_path(@field_group.category_id), notice: 'Field group was successfully created.' }
        format.json { render action: 'show', status: :created, location: @field_group }
      else
        format.html { render action: 'new' }
        format.json { render json: @field_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /field_groups/1
  # PATCH/PUT /field_groups/1.json
  def update
    respond_to do |format|
      if @field_group.update(field_group_params)
        format.html { redirect_to @field_group, notice: 'Field group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @field_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /field_groups/1
  # DELETE /field_groups/1.json
  def destroy
    category_id = @field_group.category_id
    @field_group.destroy
    respond_to do |format|
      format.html { redirect_to category_path(category_id) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_field_group
      @field_group = FieldGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def field_group_params
      params.require(:field_group).permit(:name, :category_id)
    end
end
