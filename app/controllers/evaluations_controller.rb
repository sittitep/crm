class EvaluationsController < ApplicationController
  before_action :set_evaluation, only: [:show, :edit, :update, :destroy]
  after_filter :update_score, only: [:update]

  # GET /evaluations
  # GET /evaluations.json
  def index
    @evaluations = Evaluation.all
  end

  # GET /evaluations/1
  # GET /evaluations/1.json
  def show
  end

  # GET /evaluations/new
  def new
    @evaluation = Evaluation.new
  end

  # GET /evaluations/1/edit
  def edit
  end

  # POST /evaluations
  # POST /evaluations.json
  def create
    @evaluation = Evaluation.new(evaluation_params)
    add_user_id(@evaluation)
    respond_to do |format|
      if @evaluation.save
        field_groups = @evaluation.category.field_groups
        field_groups.each do |fg|
          fg.fields.each do |f|
            field_detail = @evaluation.field_details.create(field_id: f.id)
          end
        end
        format.html { redirect_to edit_evaluation_path(@evaluation), notice: 'Evaluation was successfully created.' }
        format.json { render action: 'show', status: :created, location: @evaluation }
      else
        format.html { render action: 'new' }
        format.json { render json: @evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /evaluations/1
  # PATCH/PUT /evaluations/1.json
  def update
    respond_to do |format|
      if @evaluation.update(evaluation_params)
        format.html { redirect_to @evaluation.account, notice: 'Evaluation was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /evaluations/1
  # DELETE /evaluations/1.json
  def destroy
    account = @evaluation.account
    @evaluation.destroy
    respond_to do |format|
      format.html { redirect_to account }
      format.json { head :no_content }
    end
  end

  def update_score
    sum = 0
    @evaluation.field_details.each do |f|
      sum =+ f.value.to_i
    end
    @evaluation.score = sum
    @evaluation.save
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evaluation
      @evaluation = Evaluation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def evaluation_params
      params.require(:evaluation).permit(:name, :account_id, :category_id, :score, field_details_attributes: [:id, :value])
    end
end
