class SettingsController < ApplicationController
  before_filter :set_active_menu
  def set_active_menu
    @menu_active = action_name
  end
  def index
    redirect_to category_settings_path
  end

  def category
    @categories = Category.all
  end

  def account

  end

  def evaluation
    
  end
end
