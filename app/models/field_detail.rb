class FieldDetail < ActiveRecord::Base
  belongs_to :field
  belongs_to :account
  belongs_to :evaluation
end
