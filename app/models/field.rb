class Field < ActiveRecord::Base
  self.inheritance_column = nil
  belongs_to :field_group
  has_many :field_details, dependent: :destroy
end
