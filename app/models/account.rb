class Account < ActiveRecord::Base
  belongs_to :category
  has_many :field_details, dependent: :destroy
  has_many :evaluations, dependent: :destroy
  belongs_to :user
  
  accepts_nested_attributes_for :field_details, allow_destroy: true

  acts_as_commentable

  

  def field_detail(field_id)
    FieldDetail.find_by_account_id_and_field_id(id,field_id)
  end

  def all_fields
    return self.field_details.map{ |field_detail| {field_detail.field.name => field_detail.value}}
  end 

  def categories
    return Category.where(category_class: "Account").all.map{ |c| [c.name,c.id]}
  end 

  def header_fields
    return Account.column_names + self.all_fields.map{|h| h.keys[0]}
  end

  def all_fields_value
    custom_field_values = self.all_fields
    field_values = self.attributes
    custom_field_values.each do |field|
      field_values.merge!(field)
    end
    return field_values
  end

  def self.to_csv(accounts)
    CSV.generate do |csv|
      header = accounts.first.header_fields
      csv << header
      accounts.each do |account|
        modified_values = account.all_fields_value
        modified_values["category_id"] = Category.find_by_id(modified_values["category_id"]).name
        modified_values["user_id"] = User.find_by_id(modified_values["user_id"]).username
        csv << modified_values.values_at(*header)
      end
    end
  end

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      
      row = Hash[[header, spreadsheet.row(i)].transpose]
      row["category_id"] = Category.find_by_name(row["category_id"]).id
      row["user_id"] = User.find_by_username(row["user_id"]).id
      attributes = Hash.new
      row.keys.each do |key|
        Account.column_names.each do |c_name|
          if key == c_name
            attributes.merge!({key => row[key]})
            row.delete(key)
          end
        end
      end

      if account = Account.find_by_id(attributes["id"])
        account.update_attributes(attributes)
      else  
        account = Account.create(attributes)
      end

      field_groups = account.category.field_groups
      field_groups.each do |fg|
        fg.fields.each do |f|
          field_detail = account.field_details.find_by_field_id(f.id) || account.field_details.new(field_id: f.id)
          field_detail.attributes = {value: row[f.name]}
          field_detail.save
        end
      end
      
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path) 
    else raise "Unknown file type: #{file.original_filename}"
    end
  end
end
