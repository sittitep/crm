class FieldGroup < ActiveRecord::Base
  belongs_to :category
  has_many :fields, dependent: :destroy
end
