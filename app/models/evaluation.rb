class Evaluation < ActiveRecord::Base
  has_many :field_details, dependent: :destroy
  belongs_to :category
  belongs_to :account
  belongs_to :user
  
  accepts_nested_attributes_for :field_details, allow_destroy: true
end
