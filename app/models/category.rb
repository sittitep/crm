class Category < ActiveRecord::Base
  has_many :field_groups, dependent: :destroy
  has_many :accounts, dependent: :destroy
  has_many :evaluations, dependent: :destroy
end
