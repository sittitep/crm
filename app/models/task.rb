class Task < ActiveRecord::Base
  belongs_to :user
  before_validation :default_due_date

  acts_as_commentable
  
  def assignee
    User.find(assignee_id)
  end

  def default_due_date
    self.due_date.blank? ? self.due_date = Date.today : ""
  end

end
