class Setting
  def self.sidebar
    return ["accounts","tasks","settings"]
  end

  def self.field_option
    return ["short","long","date","boolean","select-box"]
  end

  def self.setting_menu
    return ["category"]
  end

  def self.category_class
    return ["Account"]
    # return ["Account","Evaluation"]
  end

  def self.task_status
    return ["Pending","Assigned","Completed"]
  end
end
