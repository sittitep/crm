module ApplicationHelper
  def titlelize(text)
    return text.split.map(&:capitalize).join(' ')
  end

  def date_long(date)
    return date.to_s(:long)
  end
end