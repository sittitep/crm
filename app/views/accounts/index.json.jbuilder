json.array!(@accounts) do |account|
  json.extract! account, :category_id
  json.url account_url(account, format: :json)
end
