json.array!(@evaluations) do |evaluation|
  json.extract! evaluation, :account_id, :category_id, :score
  json.url evaluation_url(evaluation, format: :json)
end
