json.array!(@tasks) do |task|
  json.extract! task, :user_id, :assignee_id, :name, :status, :due_date
  json.url task_url(task, format: :json)
end
