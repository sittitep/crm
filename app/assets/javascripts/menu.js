$(document).ready(function(){
  var ulHeight = "-"+$("ul").height()+"px"
  $(".span3").css("top", ulHeight)
  $("#toggle").on("click",function(){
    if ($(".span3").css("top") != "0px"){
      $(".span3").animate({top: "0px"}, 150)
      $("#toggle a i").removeClass("icon-chevron-down")
      $("#toggle a i").addClass("icon-chevron-up")
    }else{
      $(".span3").animate({top: ulHeight}, 150)
      $("#toggle a i").removeClass("icon-chevron-up")
      $("#toggle a i").addClass("icon-chevron-down")
    }
    
  })  
})

