// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.ui.datepicker
//= require twitter/bootstrap
//= require menu
//= require_tree .

$(document).ready(function(){
  $('.datepicker').datepicker({ dateFormat: "yy-mm-dd" });
  $(".slider-range-min").each(function(){
    var value = $(this).parent().next().find(".test").val()
    $(this).slider({
      range: "min",
      min: 0,
      max: 10,
      value: value,
      slide: function( event, ui ) {
        $(this).parent().next().find(".test").val(ui.value );
      }
    });
  });
});
