// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready(function(){
  var xhrGlobalVar;


  $('#accountCategories a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });

  $(".searchByName").on('input',function(e){
    var search = $(this);
    var table = search.parent().parent().parent().parent();
    e.preventDefault();

    accountSearch(search,xhrGlobalVar,table)
  });

});

function accountSearch(search,xhr,table) {
  if (search.val().length >= 3){
    if(xhr && xhr.readystate != 4){
      xhr.abort();
    }
    xhr = $.ajax({
            url: "/accounts",
            type: "get",
            dataType: "script",
            data: { task: "search", name: search.val(), category_id: search.data("category") },
            beforeSend: function() {
              table.find("tbody tr").remove()
              var newTr = "<tr><td><img src='http://www.acecrane.com/img/loading.gif'></td></tr>"
              table.find("tbody").append(newTr);
            }
          }).done(function( data ) {
            var data = JSON.parse(data);
            table.find("tbody tr").remove()
            $.each(data, function( index, value ) {
              var url = "/accounts/"+value["id"]
              var newTr = "<tr><td><i>#"+value["id"]+"</i><b><a href="+url+"> "+value["name"]+"</a></b><i> created by </i><b><a>"+value["username"]+"</a></b>, <i>"+value["created_at"]+"</i></td></tr>"
              table.find("tbody").append(newTr);
            });  
          });
  }
}
