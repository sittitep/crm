class TaskMailer < ActionMailer::Base
  default from: "no-reply@crm.com"
  
  def new_task(user,task)
    @user = user
    @task = task
    mail(:to => "#{user.username} <#{user.email}>", :subject => "New Task")
  end

  def completed_task(user,task)
    @user = user
    @task = task
    mail(:to => "#{user.username} <#{user.email}>", :subject => "Completed Task")
  end

  def canceled_task(user,task)
    @user = user
    @task = task
    mail(:to => "#{user.username} <#{user.email}>", :subject => "Canceled Task")
  end  

end
